# find-caeasar

A simple program to find interesting caesar ciphers.

In this case I define "interesting" as one word that becomes another when
ciphered.

![Screenshot](https://i.imgur.com/kUTVWUd.png)
