use std::{
    borrow::Cow,
    collections::hash_map::{Entry, HashMap},
    env,
    fs::File,
    io::{self, prelude::*, BufReader}
};

fn shift(s: &str, amount: i8) -> String {
    let alphabet_len = (b'z' - b'a') + 1;

    s.bytes()
        .map(|c| (b'a' + (val(c) + amount + alphabet_len as i8) as u8 % alphabet_len) as char)
        .collect()
}
fn val(c: u8) -> i8 {
    match c {
        b'a'..=b'z' => (c - b'a') as i8,
        b'A'..=b'Z' => (c - b'A') as i8,
        _ => panic!("val called on invalid character: {:?}", c as char)
    }
}

fn main() -> io::Result<()> {
    let file = env::args().skip(1).next()
        .map(Cow::Owned)
        .unwrap_or(Cow::Borrowed("/usr/share/dict/words"));
    let reader = BufReader::new(File::open(&*file)?);

    let mut map: HashMap<String, i8> = HashMap::new();

    for line in reader.lines() {
        let line = line?;
        let line = line.trim();

        if line.len() < 4 || line.bytes().any(|b| (b < b'a' || b > b'z') && (b < b'A' || b > b'Z')) {
            continue;
        }

        let start = val(line.bytes().next().unwrap());

        match map.entry(shift(line, val(b'a') - start)) {
            Entry::Vacant(placeholder) => {
                placeholder.insert(start);
            },
            Entry::Occupied(amount) => {
                let mut amount = start - amount.get();
                if amount != 0 {
                    let original = shift(line, -amount);

                    // Reduce 22 to -4 and -25 to 1, etc
                    // Prefers +13 over -13
                    let alphabet_len = (b'z' - b'a' + 1) as i8;
                    if amount > alphabet_len / 2 {
                        amount -= alphabet_len;
                    } else if amount <= -(alphabet_len / 2) {
                        amount += alphabet_len;
                    }

                    println!("{} {:+03} = {}", original, amount, line);
                }
            }
        }
    }

    Ok(())
}
